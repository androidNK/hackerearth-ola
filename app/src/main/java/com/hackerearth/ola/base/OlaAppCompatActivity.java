package com.hackerearth.ola.base;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.hackerearth.ola.R;

import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack;
import lolodev.permissionswrapper.wrapper.PermissionWrapper;

/**
 * OlaAppCompatActivity class is Base Activity.
 * Created by Kamran on 12/16/17.
 */

public abstract class OlaAppCompatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutID());

        checkPermission();

        onActivityCreate();
    }

    private void checkPermission() {
        new PermissionWrapper.Builder(this)
                .addPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE})
                //enable rationale message with a custom message
                .addPermissionRationale(getString(R.string.rationale_message))
                //show settings dialog,in this case with default message base on requested permission/s
                .addPermissionsGoSettings(true)
                //enable callback to know what option was choosen
                .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                    @Override
                    public void onGrant() {
                        Log.i("KAMRAN_LOG", this.getClass().getSimpleName() + " Permission was granted.");
                    }

                    @Override
                    public void onDenied(String permission) {
                        Log.i("KAMRAN_LOG", this.getClass().getSimpleName() + " Permission was not granted.");
                    }
                }).build().request();
    }

    @Override
    protected void onRestart() {
        checkPermission();
        super.onRestart();
    }

    protected abstract int getLayoutID();

    protected abstract void onActivityCreate();

}
