package com.hackerearth.ola;

import com.hackerearth.ola.base.OlaAppCompatActivity;

public class MainActivity extends OlaAppCompatActivity {

    @Override
    protected int getLayoutID() {
        return R.layout.activity_main;
    }

    @Override
    protected void onActivityCreate() {

    }
}
