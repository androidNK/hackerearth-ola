package com.lhtlogger;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Kamran on 3/2/17.
 * LHT logger class to log all message in console
 */

public class LHTLogger {

    private static final String U = " - ";
    private static String applicationName;
    private static LHTLogger instance;
    private static boolean mEnableLog;

    /**
     * instance method will return Shared instance of LHTLogger.
     * @return LHTLogger instance
     */
    public static LHTLogger instance() {
        if (instance == null) {
            instance = new LHTLogger();
            return instance;
        }

        return instance;
    }

    private LHTLogger() {
    }

    /**
     * @param appName   name of the Application
     * @param enableLog true to print log.
     */
    public static void init(String appName, boolean enableLog) {
        applicationName = appName;
        mEnableLog = enableLog;
    }

    /**
     * To Print debug log in Android Monitor console
     *
     * @param developerName  name of developer
     * @param className      name of the class used in
     * @param methodName     name of the method used in.
     * @param message        string message
     * @param object         any type of object
     * @param writeLogToFile true to write log to file
     */
    public void debug(String developerName, String className, String methodName, String message, Object object, boolean writeLogToFile) {
        if (applicationName == null)
            throw new LhtLogNotInitialisedException();

        else if (mEnableLog) {
            message = getMessage(developerName, className, methodName, message, object);
            Log.d("LHT_DEBUG_LOG", message);
            if (writeLogToFile)
                writeLogToFile(message);
        }
    }

    /**
     * To Print error log in Android Monitor console
     *
     * @param developerName  name of developer
     * @param className      name of the class used in
     * @param methodName     name of the method used in.
     * @param message        string message
     * @param object         any type of object
     * @param writeLogToFile true to write log to file
     */
    public void error(String developerName, String className, String methodName, String message, Object object, boolean writeLogToFile) {
        if (applicationName == null)
            throw new LhtLogNotInitialisedException();

        else if (mEnableLog) {
            message = getMessage(developerName, className, methodName, message, object);

            Log.e("LHT_ERROR_LOG", message);
            if (writeLogToFile)
                writeLogToFile(message);
        }
    }

    /**
     * To Print verbose log in Android Monitor console
     *
     * @param developerName  name of developer
     * @param className      name of the class used in
     * @param methodName     name of the method used in.
     * @param message        string message
     * @param object         any type of object
     * @param writeLogToFile true to write log to file
     */
    public void verbose(String developerName, String className, String methodName, String message, Object object, boolean writeLogToFile) {
        if (applicationName == null)
            throw new LhtLogNotInitialisedException();

        else if (mEnableLog) {
            message = getMessage(developerName, className, methodName, message, object);
            Log.v("LHT_VERBOSE_LOG", message);
            if (writeLogToFile)
                writeLogToFile(message);
        }
    }

    /**
     * To Print Warning log in Android Monitor console
     *
     * @param developerName  name of developer
     * @param className      name of the class used in
     * @param methodName     name of the method used in.
     * @param message        string message
     * @param object         any type of object
     * @param writeLogToFile true to write log to file
     */
    public void warning(String developerName, String className, String methodName, String message, Object object, boolean writeLogToFile) {
        if (applicationName == null)
            throw new LhtLogNotInitialisedException();

        else if (mEnableLog) {
            message = getMessage(developerName, className, methodName, message, object);
            Log.w("LHT_WARN_LOG", message);
            if (writeLogToFile)
                writeLogToFile(message);
        }
    }

    @NonNull
    private String getMessage(String developerName, String className, String methodName, String message, Object object) {
        StringBuilder messageBuilder = new StringBuilder();

        messageBuilder.append(developerName)
                .append(U)
                .append(applicationName)
                .append(U)
                .append(className)
                .append(U)
                .append(methodName)
                .append(U)
                .append(message);

        if (object != null)
            messageBuilder.append("\n").append(object.toString());

        return messageBuilder.toString();
    }

    private void writeLogToFile(String message) {
        if (isExternalStorageWritable()) {
            message = new Date().toString() + ": " + message;
            getAlbumStorageDir(applicationName + "LOG", message);
        }

    }

    /* Checks if external storage is available for write */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private File getAlbumStorageDir(String albumName, String message) {
        // Get the directory for the user's public pictures directory.
        BufferedWriter bw = null;
        FileWriter fw = null;
        boolean isFileCreated;
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), albumName + ".txt");
        try {
            isFileCreated = file.exists();
            if (!isFileCreated) {
                isFileCreated = file.createNewFile();
            }

            if (isFileCreated) {
                // true = append file
                fw = new FileWriter(file.getAbsoluteFile(), true);
                bw = new BufferedWriter(fw);

                bw.write(message + "\n\n");
            }

        } catch (IOException e) {
            Log.e("EXCEPTION", "LHTLogger getAlbumStorageDir: \n" + Arrays.toString(e.getStackTrace()));
        } finally {
            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {
                Log.e("EXCEPTION", "LHTLogger getAlbumStorageDir_finally: \n" + Arrays.toString(ex.getStackTrace()));
            }
        }
        return file;
    }

}