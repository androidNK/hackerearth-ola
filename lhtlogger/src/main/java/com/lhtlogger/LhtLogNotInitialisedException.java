package com.lhtlogger;

/**
 * Created by Kamran on 22/08/17.
 * LhtLogNotInitialisedException is RuntimeException if user don't initialise the LhtLogger
 */
class LhtLogNotInitialisedException extends RuntimeException {

    LhtLogNotInitialisedException() {
        super("LHTLogger is not initialised.\n Initialise in Application Class, Syntax to initialise \"LHTLogger.init(appName,enableLog);");
    }
}